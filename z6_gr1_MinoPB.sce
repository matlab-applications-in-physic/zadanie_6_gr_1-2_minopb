// Author : __MinoPB__   
// environment : SCILAB 
// Date of creation: 2019-11-12
// Have a nice day sir (or madam (: )!

//setting a clock
dt = getdate()

//stations id 
MainIMGW_url = 'https://danepubliczne.imgw.pl/api/data/synop/id/'
IMGW_bielsko = '12600'
IMGW_katowice = '12560'
IMGW_czestchowa = '12550'
//FormatIMGW_url = '/format/json'

MainWttr_url = 'http://wttr.in/'
Wttr_bielsko = 'bielsko'
Wttr_katowice = 'katowice'
Wttr_czestochowa = 'czestochowa'
FormatWttr_url = '?format=j1'

IMGW_mat = [IMGW_bielsko IMGW_katowice IMGW_czestchowa]
Wttr_mat = [Wttr_bielsko Wttr_katowice Wttr_czestochowa]


IMGW_Js_Ct = ['' '' '']

//while 1 
    //if dt(8) == 1
        for j = 1:3;                                    //structure buliding for IMGW data
            k = IMGW_mat(1,j);
            URL = MainIMGW_url + k
            content = getURL(URL)
            IMGW_Js_Ct(j) = content
            IMGW_struct = JSONParse(mgetl(content))           
            mopen('IMGW_results','a')
            IMGW_str = ['Data Pomiaru : ',IMGW_struct.data_pomiaru, 'godzina pomiaru :', IMGW_struct.godzina_pomiaru, 'stacja:',IMGW_struct.stacja, 'temperatura :', IMGW_struct.temperatura,'cisnienie :', IMGW_struct.cisnienie,'wilgotność względna : ', IMGW_struct.wilgotnosc_wzgledna,'Prędkość wiatru :', IMGW_struct.predkosc_wiatru];
            IMGW_com_str = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
            IMGW_CSV_id = 'weather' + IMGW_struct.data_pomiaru + '.csv'
            mopen(IMGW_CSV_id, 'a')
            csvWrite(IMGW_str, IMGW_CSV_id, ';', '.',[],IMGW_com_str)
            
            write('IMGW_results', IMGW_str, IMGW_T)
            mclose('all')
            
            
            
            t = Wttr_mat(1,j);                           //structure buliding for Wttr.in data
            URL = MainWttr_url + t + FormatWttr_url
            content = getURL(URL)
            Wttr_Js_Ct(j) = content
            Wttr_struct = JSONParse(mgetl(content))
                        
            
            fd = mopen('WTTR_results','a')
            Wttr_str = ['Data Pomiaru : ', IMGW_struct.data_pomiaru,  'godzina pomiaru :', Wttr_struct.current_condition.observation_time, 'stacja:',t , 'temperatura :', Wttr_struct.current_condition.temp_C, 'cisnienie :', Wttr_struct.current_condition.pressure, 'wilgotność względna : ', Wttr_struct.current_condition.humidity, 'Prędkość wiatru :', Wttr_struct.current_condition.windspeedKmph,'Pobrana temperatura odczuwalna :', Wttr_struct.current_condition.FeelsLikeC];
            
           write('WTTR_results', Wttr_str, Wttr_T)
           mclose('all')
           
           
        
        end 
    //end  
//end


  



